package ntu.csie.oop13spring;
import java.util.*;

public abstract class POOSkill
{
    /**
       the skill is defined as "something"
       that can change the HP or AGI of any POOPet
    */
    public abstract void act(POOPet pet);
}

class POOTinyAttackSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        int hp = pet.getHP();
        if (hp > 0)
            if(!pet.setHP(hp - 1))
                pet.setHP(0);
        System.out.println("TinyAttack!");
        System.out.println(pet.getName()+" loses 1 HP.");
    }
}
class POOCureSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int hp = pet.getHP();
        int rand=ran.nextInt(5);
        if (hp > 0)
            if(!pet.setHP(hp + 1+ran.nextInt(5)))
                pet.setHP(0);
        System.out.println("Cure!");
        System.out.println("Gain "+(1+rand)+" HP.");
    }
}
class POOSlashSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int hp = pet.getHP();
        int rand=ran.nextInt(10);
        if (hp > 0)
            if(!pet.setHP(hp -20-rand))
                pet.setHP(0);
        System.out.println("Slash!");
        System.out.println(pet.getName()+" loses "+(20+rand)+" HP. ");
    }
}
class POOLuckyAttackSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int hp = pet.getHP();
        int rand=ran.nextInt(5);
        int lucky=ran.nextInt(10)+1;
        rand+=35;
        System.out.println("LuckyAttack!!");
        if(lucky<=3)
        {   
            System.out.println("Double damage!!!!!!");
            rand*=2;
        }
        if (hp > 0)
            if(!pet.setHP(hp -rand))
                pet.setHP(0);
        System.out.println(pet.getName()+" loses "+(rand)+" HP. ");
    }
}
class POORecoverySkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int hp = pet.getHP();
        int rand=ran.nextInt(10);
        if (hp > 0)
            if(!pet.setHP(hp + 10+rand))
            {
                System.out.println("Can't gain more hp!");
                return ;
            }
        System.out.println("Recovery!");
        System.out.println(pet.getName()+" Gain "+(10+rand)+" HP.");
    }
}
class POORestore_MPSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int mp = pet.getMP();
        int rand=ran.nextInt(5);
        if (mp >=0)
            pet.setMP(mp + 10+rand);
        System.out.println("Restore_MP!");
        System.out.println(pet.getName()+" Gain "+(10+rand)+" MP.");
    }
}
class POODouble_arrowsSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int hp = pet.getHP();
        int rand=ran.nextInt(10);
        if (hp > 0)
            if(!pet.setHP(hp -40-rand))
                pet.setHP(0);
        System.out.println("Double arrows!!");
        System.out.println(pet.getName()+" loses "+(40+rand)+" HP. ");
    }
}
class POOSniper_arrowSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int hp = pet.getHP();
        int rand=ran.nextInt(5);
        if (hp > 0)
            if(!pet.setHP(hp -15-rand))
                pet.setHP(0);
        System.out.println("Sniper arrow...");
        System.out.println(pet.getName()+" loses "+(15+rand)+" HP. ");
    }
}
class POOMagic_arrowSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int hp = pet.getHP();
        int mp = pet.getMP();
        int rand=ran.nextInt(5);
        int randm=ran.nextInt(5);
        if (hp > 0)
            if(!pet.setHP(hp -5-rand))
                pet.setHP(0);
        if (mp > 0)
            pet.setMP(mp -10-randm);
        System.out.println("Magic arrow!?");
        System.out.println(pet.getName()+" loses "+(5+rand)+" HP. ");
        System.out.println("Simultaneously, "+pet.getName()+" loses "+(10+randm)+" MP. ");
    }
}
class POORestore_MP_ArcherSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int mp = pet.getMP();
        int rand=ran.nextInt(10);
        if (mp >=0)
            pet.setMP(mp + 10+rand);
        System.out.println("Restore_MP!");
        System.out.println(pet.getName()+" Gain "+(10+rand)+" MP.");
    }
}
class POOPikeAttackSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int hp = pet.getHP();
        int rand=ran.nextInt(10);
        if (hp > 0)
            if(!pet.setHP(hp -15-rand))
                pet.setHP(0);
        System.out.println("PikeAttack!");
        System.out.println(pet.getName()+" loses "+(15+rand)+" HP. ");
    }
}
class POOSuicideAttackSkill extends POOSkill
{
    public void act(POOPet pet)
    {
        Random ran = new Random();
        int hp = pet.getHP();
        int rand=ran.nextInt(15);
        if (hp > 0)
            if(!pet.setHP(hp -35-rand))
                pet.setHP(0);
        System.out.println("PikeAttack!");
        System.out.println(pet.getName()+" loses "+(35+rand)+" HP. ");
    }
  
}