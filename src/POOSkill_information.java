package ntu.csie.oop13spring;
import java.util.*;
public class POOSkill_information 
{
    private int mana,distance,hpuse;
    private boolean enemy;
    String skill_name,skill_classname;
    public POOSkill_information(int mana,int distance,int hpuse,boolean enemy,String skill_name,String skill_classname)
    {
        this.mana=mana;
        this.distance=distance;
        this.hpuse=hpuse;
        this.enemy=enemy;
        this.skill_name=skill_name;
        this.skill_classname=skill_classname;
    }
    /**
        get mana usage
    */
    public int getmana()
    {
        return mana;
    }
    /**
        get skill's distance
    */
    public int getdistance()
    {
        return distance;
    }
    /**
        get health power usage
    */
    public int gethpuse()
    {
        return hpuse;
    }
    /**
        get this skill's target is enemy or ally
    */
    public boolean getenemy()
    {
        return enemy;
    }
    /**
        get this skill's name
    */
    public String getskill_name()
    {
        return skill_name;
    }
    /**
        get this skill's class name which extends POOSkill
    */
    public String getskill_classname()
    {
        return skill_classname;
    }
}