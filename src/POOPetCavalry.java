package ntu.csie.oop13spring;
import java.util.*;

public class POOPetCavalry extends POOPet
{
    private int HP, MP, AGI;
    private String name;

    public POOPetCavalry()
    {
    	Scanner scanner = new Scanner(System.in);
    	Random ran = new Random();
    	System.out.print("Please input the Cavalry_pet's name(at most 8 characters): ");
        setName(scanner.next());
        setHP(90+ran.nextInt(15));
        setMP(35+ran.nextInt(10));
        setAGI(7+ran.nextInt(1));
    }

 
    /**
       act defines how the pet would choose a pet
       on the arena (including possibly itself)
       and determine a skill to be used on
       the pet
    */
    protected POOAction act(POOArena arena)
    {
        Scanner scanner = new Scanner(System.in);
        String inputname;
        POOArenakind a=(POOArenakind)arena;
        POOAction action=new POOAction();
        POOCoordinatekind c=new POOCoordinatekind(arena.getPosition(this));
        int choice;
        POOSkill_information[] s_i=new POOSkill_information[5];
        s_i[1]=new POOSkill_information(10,2,0,true,"PikeAttack","ntu.csie.oop13spring.POOPikeAttackSkill");
        s_i[2]=new POOSkill_information(15,1,20,true,"SuicideAttack","ntu.csie.oop13spring.POOSuicideAttackSkill");
        s_i[3]=new POOSkill_information(10,1000,0,false,"Heal HP","ntu.csie.oop13spring.POORecoverySkill");
        s_i[4]=new POOSkill_information(0,1000,10,false,"Heal MP","ntu.csie.oop13spring.POORestore_MPSkill");
        System.out.println("Skill list: ");
        System.out.println("1. PikeAttack    Damage:20 MPuse:10   Target:enemy in 2 distance");
        System.out.println("2. SuicideAttack Damage:42 MPuse:15   Target:enemy in 1 distance(hurt 20 HP itself)");
        System.out.println("3. Heal HP       HP:15     MPuse:15   Target:any ally on map(including itself)");
        System.out.println("4. Heal MP       MP:12     HPuse:15   Target:any ally on map(including itself)");
        System.out.println("5. Do nothing ");
        System.out.print("Please input the skill you want to use: ");
        while(true)
        {
            choice=scanner.nextInt();
            if(choice>=1&&choice<=5)
            {
                if(choice==5)
                    return null;
                else if(getHP()<s_i[choice].gethpuse())
                    System.out.println("Out of HP. Remain HP is "+getHP()+" but the HP usage of this skill is "+s_i[choice].gethpuse());
                else if(getMP()<s_i[choice].getmana())
                    System.out.println("Out of MP. Remain MP is "+getMP()+" but the MP usage of this skill is "+s_i[choice].getmana());
                else if(!targetexist(arena,a,c,s_i[choice].getdistance(),s_i[choice].getenemy())) 
                    System.out.println("There is no any target on map in distance "+s_i[choice].getdistance());
                else
                    break;
            }
            else
                System.out.println("Invalid value. Should input 1~4.");
            System.out.println("Input again. ");
        }
        a.screenclean();
        while(true)
        {
            if (actcheck(arena,a,action,c,s_i[choice])) 
                break;
            else
                a.screenclean();
        }
       return action;
    }
/**
    Make sure there is a target for the pet to use skill by skill's distance
*/
    private boolean targetexist(POOArena arena,POOArenakind a,POOCoordinatekind c,int distance,boolean enemy) 
    {
        if(!enemy)
            return true;
        for(int i=0;i<a.getn();++i)
            for(int j=0;j<a.getm();j++)
            {
                if(a.map[i][j]!=null)
                    if(!(arena.getAllPets()[a.getPetorder(a.map[i][j])] instanceof POOPetCavalry))
                        if(Math.abs(c.x-j)+Math.abs(c.y-i)<=distance)
                            return true;
            }
        return false;
    }
/**
    act and construct POOAction, return this act legitimate or not
*/
    private boolean actcheck(POOArena arena,POOArenakind a,POOAction action,POOCoordinatekind c,POOSkill_information s_i)
    {
        a.printmap_skill_available(c.x,c.y,s_i.getdistance(),!s_i.getenemy(),1);
        Scanner scanner = new Scanner(System.in);
        System.out.println(getName()+" will use "+s_i.getskill_name()+". Which pet is his target? ");
        System.out.print("Please input the pet's name: ");
        String inputname=scanner.next();
        if(a.getPetorder(inputname)==-1)
            System.out.println("Can't find "+inputname+" on the map.");
        else if(s_i.getenemy()&&(arena.getAllPets()[(a.getPetorder(inputname))] instanceof POOPetCavalry))
            System.out.println("Can't "+s_i.getskill_name()+" the same kind pet. ");
        else if(!s_i.getenemy()&&!(arena.getAllPets()[(a.getPetorder(inputname))] instanceof POOPetCavalry))
            System.out.println("Can't "+s_i.getskill_name()+" the different kind pet. ");
        else if(arena.getAllPets()[(a.getPetorder(inputname))].getHP()<=0)
            System.out.println("Can't "+s_i.getskill_name()+" the dead pet. ");
        else
        {
            POOCoordinate target=a.getPosition(arena.getAllPets()[(a.getPetorder(inputname))]);
            if(Math.abs(target.x-c.x)+Math.abs(target.y-c.y)<=s_i.getdistance())
            {
                int mp=getMP();
                int hp=getHP();
                setMP(mp-s_i.getmana());
                setHP(hp-s_i.gethpuse());
                try
                {
                    action.skill=(POOSkill)Class.forName(s_i.getskill_classname()).newInstance();
                }
                catch(Exception e)
                {
                    System.out.println(e);
                    return false;
                }
                action.dest=arena.getAllPets()[(a.getPetorder(inputname))];
                return true;
            }
            else
                System.out.println(inputname+" is out of range. Only pets which is '*' behind can be the target.");
        }
        System.out.println("Please input again."); 
        return false;
    }
    /**
       move defines how the pet would want to move in an arena;
       note that the range of moving should be related to AGI
     */
    protected POOCoordinate move(POOArena arena)
    {
        Scanner scanner = new Scanner(System.in);
        while(true)
        {
            System.out.println(getName()+"'s AGI is "+getAGI()+" . Therefore, you can at most move "+getAGI()+" steps.");
            System.out.println("Please input the delta x and delta y you want to move: ");
            System.out.print("Delta x(none sign for right and '-' for left): ");
            int x=scanner.nextInt();
            System.out.print("Delta y(none sign for down and '-' for up): ");
            int y=scanner.nextInt();
            if(Math.abs(x)+Math.abs(y)>getAGI())
            {
                System.out.println("Steps are more than +"+getAGI()+" . Invalid movement. ");
                System.out.println("Please input (x,y) again.");
                continue;
            }
            POOCoordinatekind c=new POOCoordinatekind(arena.getPosition(this));
            if(x==0&&y==0)
            {
                System.out.println(getName()+" stays in the same place. ");
                return new POOCoordinatekind(c.x,c.y);      
            }
            POOArenakind a=(POOArenakind)arena;
            if(c.indistance(x,y,a.getm(),a.getn()))
                if(!a.checkexist(c.y+y,c.x+x))
                {
                    a.map[c.y+y][c.x+x]=getName();
                    a.map[c.y][c.x]=null;
                    System.out.println(getName()+" moves to ("+(c.x+x+1)+", "+(c.y+y+1)+"). ");
                    return new POOCoordinatekind(c.x+x,c.y+y);
                }
            
        }
        
    }
}
