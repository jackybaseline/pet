package ntu.csie.oop13spring;

import java.util.*;

public class POOArenakind extends POOArena
{
    static private boolean checksamename;
    static private boolean printend;
    private int m;
	private int n;
	protected String map[][];
	private int round;
	private ArrayList<samename> stringlist = new ArrayList<samename>(0);
	POOArenakind()
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please input the arena's size(m*n): ");
		System.out.print("m: ");
		m=scanner.nextInt();
		System.out.print("n: ");
		n=scanner.nextInt();
		map=new String[n][];
		for(int i=0;i<n;++i)
			map[i]=new String[m];
		round=1;
	}
	/**
		get array of samename
	*/
	private samename[] getsamenamearray()
	{
        samename[] s = new samename[0];
        return stringlist.toArray(s);
	}
	/**
	a class for checking any pets are same name or not.
	If yes, +repeatname.
	Eg: Jacky Jacky Jacky-> Jacky Jacky1 Jacky2 
	*/
	private class samename
	{
		private String name;
		private int times;
		samename(String name,int times)
		{
			this.name=name;
			this.times=times;
		}
		public String getName()
		{
			return name;
		}
		public void moretimes()
		{
			++times;
		}	
		public int getTimes()
		{
			return times;
		}	
	}
	/**
	Check this arena keeps fighting or not
	*/
	public boolean fight()
	{
		int check=-1,i;
		for(i=0;i<getAllPets().length;++i)
		{
			if(getAllPets()[i].getHP()<=0);
			else if(check==-1)
				check=checkkind(i);
			else if(check!=checkkind(i))
				return true;
		}
		if(!POOArenakind.printend)
		{
			printmap();
			switch(check)
			{
				case 0:
					System.out.println("Infantry won!");
					break;
				case 1:
					System.out.println("Cavalry won!");
					break;
				case 2:
					System.out.println("Archer won!");
					break;
			}
		}
		POOArenakind.printend=true;
		return false;
	}
	/**
		Run and show this arena
	*/
    public void show()
	{
		Scanner scanner = new Scanner(System.in);
		int j;
		if(!POOArenakind.checksamename)
		{
			for(int i=0;i<getAllPets().length;++i)
			{
				for(j=0;j<getsamenamearray().length;j++)
				{
					if(getAllPets()[i].getName().compareTo(getsamenamearray()[j].getName())==0)
					{
						getsamenamearray()[j].moretimes();
						getAllPets()[i].setName(getsamenamearray()[j].getName()+Integer.toString(getsamenamearray()[j].getTimes()));
						break;
					}
				}
				if(j==getsamenamearray().length)
				{
					samename s=new samename(getAllPets()[i].getName(),0);
					stringlist.add(s);
				}
				POOArenakind.checksamename=true;
			}
		}
		POOArenakind.checksamename=true;
		for(int i=0;i<getAllPets().length;++i)
		{
			if(!fight())
				return ;
			if(getAllPets()[i].getHP()<=0)
				continue;
			System.out.println("Pet fight game! One kind will win if the rest of pets are the same kind.");
			System.out.println("Round "+round);
			if(getPosition(getAllPets()[i])==null)
			{
				printmap();
				printpet(i);
				System.out.println(getAllPets()[i].getName()+" is not on the map. Please input its position(x,y): ");
			 	while(true)
			 	{
			 		System.out.print("x: ");
			 		int x=scanner.nextInt();
			 		System.out.print("y: ");
			 		int y=scanner.nextInt();
			 		if(!checkexist(y-1,x-1))
			 		{
						map[y-1][x-1]=getAllPets()[i].getName();
						break;
			 		}
			 	}
			}
			else
			{
				POOCoordinate c=getPosition(getAllPets()[i]);
				printmap_move_available(c.x,c.y,getAllPets()[i].getAGI());
				printpet(i);
				System.out.println("It's your turn, "+getAllPets()[i].getName()+".");
				System.out.println("Move first. '*' means you can move there.");
				getAllPets()[i].move(this);
				screenclean();
				printmap();
				System.out.println("Then act.");
				printpet(i);
				POOAction action=getAllPets()[i].act(this);
				if(action!=null)
				{
					action.skill.act(action.dest);
					System.out.println(getAllPets()[i].getName()+" changes into: ");
					printpet(i);
					System.out.println("The target changes into: ");
					printpet(getPetorder(action.dest.getName()));
					if(action.dest.getHP()<=0)
						if(deletepet(action.dest.getName()))
							System.out.println(action.dest.getName()+" is dead.");
				}
			}
			screenclean();
		}
		for(int i=0;i<getAllPets().length;++i)
		{
			int mp=getAllPets()[i].getMP();              //cure MP
			getAllPets()[i].setMP(mp+5);
		}	
		System.out.println("Since round ends, each pet gets 5 MP regain.");
		++round;
		
	}
	/**
		return the pet's postion by parameter pet
	*/
    public POOCoordinate getPosition(POOPet p)
	{
		for(int i=0;i<n;++i)
			for(int j=0;j<m;++j)
			{
				if(map[i][j]==null);
				else if(map[i][j].compareTo(p.getName())==0)
					return new POOCoordinatekind(j,i);
			}
		return null;
	}
	/**
	 	list all the pets on the arena
	*/
	public void printpetlist()
	{
		System.out.println("Pet list: ");
		for(int i=0;i<getAllPets().length;++i)
			System.out.printf("Name: %-8s ",getAllPets()[i].getName());
		System.out.println("");
		for(int i=0;i<getAllPets().length;++i)
			System.out.printf("HP  : %-8d ",getAllPets()[i].getHP());
		System.out.println("");
		for(int i=0;i<getAllPets().length;++i)
			System.out.printf("MP  : %-8d ",getAllPets()[i].getMP());
		System.out.println("");
		for(int i=0;i<getAllPets().length;++i)
			System.out.printf("AGI : %-8d ",getAllPets()[i].getAGI());
		System.out.println("");
		for(int i=0;i<getAllPets().length;++i)
		{
			System.out.printf("Team: ");
			switch(checkkind(i))
			{
				case 0:
					System.out.print("Infantry ");
					break;
				case 1:
					System.out.print("Cavalry  ");
					break;
				case 2:
					System.out.print("Archer   ");
					break;
			}
		}
		System.out.println("");
	}
	/**
		print certain pet
	*/
	public void printpet(int i)
	{
		System.out.println("Name: "+getAllPets()[i].getName());
		System.out.println("HP  : "+getAllPets()[i].getHP());
		System.out.println("MP  : "+getAllPets()[i].getMP());
		System.out.println("AGI : "+getAllPets()[i].getAGI());
		System.out.print("Team: ");
		switch(checkkind(i))
		{
			case 0:
				System.out.println("Infantry");
				break;
			case 1:
				System.out.println("Cavalry");
				break;
			case 2:
				System.out.println("Archer");
				break;
		}
	}
	/**
		print the map if is a pet on a certain point,
		print its name by String and team by character
		Eg: Jacky-A   (A:Archer)
	*/
	private void printmap()
	{
		printpetlist();
		char kind='F';
		for(int i=0;i<m;++i)
			System.out.printf("%6d     ",i+1);
		System.out.println("");
		System.out.println("");
		for(int i=0;i<n;++i)
		{
			System.out.printf("%d",i+1);
			for(int j=0;j<m;++j)
			{
				if(map[i][j]==null)
					System.out.printf("%5d-%-5d",j+1,i+1);
				else
				{
					switch(checkkind(getPetorder(map[i][j])))
					{
						case 0:
							kind='I';
							break;
						case 1:
							kind='C';
							break;
						case 2:
							kind='A';
							break;
					}
					System.out.printf("%8s-%c ",getAllPets()[getPetorder(map[i][j])].getName(),kind);
				}
			}
			System.out.println("");
			System.out.println("");
		}
		System.out.println("I: Infantry, C: Cavalry, A:  Archer");
	}
	/**
		print the map if is a pet on a certain point,
		print its name by String and team by character
		Eg: Jacky-A   (A:Archer)
		Add a functional that can print where the pet's can move to. (by agi)
		If the position is available for the pet, it will print '*' behind the coordinate.
		Eg: 3-5     *   (available )
	*/
	public void printmap_move_available(int x,int y,int agi)
	{
		printpetlist();
		char kind='F';
		for(int i=0;i<m;++i)
			System.out.printf("%6d     ",i+1);
		System.out.println("");
		System.out.println("");
		for(int i=0;i<n;++i)
		{
			System.out.printf("%d",i+1);
			for(int j=0;j<m;++j)
			{
				if(map[i][j]==null)
				{
					if(Math.abs(y-i)+Math.abs(x-j)<=agi)
						System.out.printf("%5d-%-4d*",j+1,i+1);
					else
						System.out.printf("%5d-%-5d",j+1,i+1);
				}
				else
				{
					switch(checkkind(getPetorder(map[i][j])))
					{
						case 0:
							kind='I';
							break;
						case 1:
							kind='C';
							break;
						case 2:
							kind='A';
							break;
					}
					System.out.printf("%8s-%c ",getAllPets()[getPetorder(map[i][j])].getName(),kind);
				}
			}
			System.out.println("");
			System.out.println("");
		}
		System.out.println("I: Infantry, C: Cavalry, A:  Archer");
	}
	/**
		print the map if is a pet on a certain point,
		print its name by String and team by character
		Eg: Jacky-A   (A:Archer)
		Add a functional that can print where the pet's can use skill to. (by skill distance)
		If the pet is available to be target, it will print '*' behind the pet's name.
		Eg: Jacky-A*   (available )
	*/
	public void printmap_skill_available(int x,int y,int distance,boolean ally,int team)
	{
		printpetlist();
		char kind='F';
		for(int i=0;i<m;++i)
			System.out.printf("%6d     ",i+1);
		System.out.println("");
		System.out.println("");
		for(int i=0;i<n;++i)
		{
			System.out.printf("%d",i+1);
			for(int j=0;j<m;++j)
			{
				if(map[i][j]==null)
					System.out.printf("%5d-%-5d",j+1,i+1);
				else
				{
					switch(checkkind(getPetorder(map[i][j])))
					{
						case 0:
							kind='I';
							break;
						case 1:
							kind='C';
							break;
						case 2:
							kind='A';
							break;
					}
					if(!ally&&checkkind(getPetorder(map[i][j]))==team)
						System.out.printf("%8s-%c ",getAllPets()[getPetorder(map[i][j])].getName(),kind);
					else if(ally&&checkkind(getPetorder(map[i][j]))!=team)
						System.out.printf("%8s-%c ",getAllPets()[getPetorder(map[i][j])].getName(),kind);
					else if(Math.abs(y-i)+Math.abs(x-j)<=distance)
						System.out.printf("%8s-%c*",getAllPets()[getPetorder(map[i][j])].getName(),kind);
					else
						System.out.printf("%8s-%c ",getAllPets()[getPetorder(map[i][j])].getName(),kind);
				}
			}
			System.out.println("");
			System.out.println("");
		}
		System.out.println("I: Infantry, C: Cavalry, A:  Archer");
		System.out.println("'*' means your available target. ");
	}
	/**
		get certain pets is belong to which team
	*/
	private int checkkind(int i)
	{
		if(getAllPets()[i] instanceof POOPetInfantry)
			return 0;
		else if(getAllPets()[i] instanceof POOPetCavalry)
			return 1;
		else if(getAllPets()[i] instanceof POOPetArcher)		
			return 2;
		return -1;
	}
	/**
	   A stop screen until input enter
	   Then clear the screen
	*/
	public void screenclean()
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print("Press enter to continue....");
 		scanner.hasNextLine();
		for(int j=0;j<50;++j)
			System.out.println("");
	}
	/**
		get the pet's order in getAllPets by its name
	*/
	public int getPetorder(String name)
	{
		for(int i=0;i<getAllPets().length;++i)
		{	
			if(getAllPets()[i].getName().compareTo(name)==0)
				return i;
		}
		System.out.println("There is no "+name+" on the map. ");
		return -1;
	}
	/**
		get m (the map's width)
	*/
	public int getm()
	{
		return m;
	}
	/**
		get m (the map's length)
	*/
	public int getn()
	{
		return n;
	}
	/**
		check the coordinate on map exist a pet or not
	*/
	public boolean checkexist(int x,int y)
	{
		if(x>=n||x<0||y>=m||y<0)
		{
	 		System.out.println("( "+(y+1)+", "+(x+1)+"). is out of range. ");
	 		System.out.println("Please input (x,y) again.");
	 		return true;
		}
		if(map[x][y]==null)
			return false;
 		System.out.println("There is already a pet "+map[x][y]+" on ( "+(y+1)+", "+(x+1)+"). ");
 		System.out.println("Please input (x,y) again.");
 		return true;
	}
	/**
		delete a dead pet on the map
	*/
	private boolean deletepet(String name)
	{
		for(int i=0;i<n;++i)
			for(int j=0;j<m;++j)
			{	
				if(map[i][j]==null)
					continue;
				else if(map[i][j].compareTo(name)==0)
				{	
					map[i][j]=null;
					return true;
				}
			}
		return false;
	}
}
