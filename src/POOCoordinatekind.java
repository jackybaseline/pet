package ntu.csie.oop13spring;

public class POOCoordinatekind extends POOCoordinate
{
    POOCoordinatekind(POOCoordinate p)
	{
		x=p.x;
		y=p.y;
	}
    POOCoordinatekind(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	/**
		check two coordinate are equals or not
	*/
	public boolean equals(POOCoordinate other)
	{
		return (other.x==x&&other.y==y) ? true:false;
	}
	/**
		check this move is legitimate or not
	*/
	public boolean indistance(int x,int y,int m,int n)
	{
		if(this.x+x>=m||this.x+x<0||this.y+y>=n||this.y+y<0)
		{
			System.out.println("("+(this.x+x+1)+", "+(this.y+y+1)+") is out of range.");
			System.out.println("Please input (x,y) again.");
			return false;
		}
		return true;
	}
}
